VL-TOOLCHAIN SUBMODULE
======================

This is a submodule to the VLCore/vlNN repositories.  Contains
the build source and sequence for the toolchain.

This module is not implemented in 7.1, so the veclinux-7.1 branch
will remain empty, while the veclinux-7.1 and forward will have
build code.
